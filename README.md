## Helm charts repository

## What is Helm?
The package manager for Kubernetes
https://helm.sh/

## Documentation:
https://helm.sh/docs/

## Helm commands:
https://helm.sh/docs/helm/

## Explore the files hosted and adapt as needed
To deploy a helm chart execute:
<p>helm install chart-name ./local-folder
<p>To delete a helm chart deployment execute:
<p>helm uninstall chart-name

## A chart deployment creates pods. 
To check status of the pods use:
<p>kubectl get pods
<p>To check logs:
<p>kubectl logs pod-name (from the list)
